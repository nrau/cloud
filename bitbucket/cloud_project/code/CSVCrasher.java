/*Crashed ParkMap1 for Manhattan only. Use LAT to fetch and calculate distances. Get approx 3500 entries per LAT. Cannot help this though. 
    Find distances and return closest 5/7 signs as JSON object from method
 */


import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

public class CSVCrasher {
    
    public static void main(String[] args) throws Exception {
        int counter = 0;
        CsvReader reader = new CsvReader("/Users/nachirau/Desktop/cloud_project/ParkMap1.csv");
        CsvWriter writer = new CsvWriter("/Users/nachirau/Desktop/cloud_project/Park_Manhattan.csv");
        String[] myHeader = {"SL", "SIGN", "X", "Y", "LAT","RULE" };
        writer.writeRecord(myHeader);
        if(reader.readHeaders()) {
            while(reader.readRecord()) {
                String[] rec = reader.getValues();
                if(!rec[reader.getIndex("SG_KEY_BOR")].equals("M")) continue;
                if(rec[reader.getIndex("SIGNDESC1")]==null ||
                   !rec[reader.getIndex("SIGNDESC1")].contains("PARKING")) continue;
                String Lat = rec[reader.getIndex("Y")];
                String Longit = rec[reader.getIndex("X")];
                
                double Latit = Double.valueOf(Lat);
                int temp = (int) (Latit*100);
                Latit = temp/100d;
                String[] thisRec = { String.valueOf(counter), rec[reader.getIndex("SG_ORDER_N")] ,
                    Longit, Lat,
                    String.valueOf(Latit),
                    rec[reader.getIndex("SIGNDESC1")]};
                writer.writeRecord(thisRec);
                counter++;				
            }
            writer.close();
        }
    }
}
