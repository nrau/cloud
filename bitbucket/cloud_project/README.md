# Car Parking Application
Sample application demonstrating the following features:

Find Parking
Validate parking

The project is built on Android Studio v1.2. We use Gradle to build the project to simplify the project packaging. Dependencies are specified in the app build file. We include external libraries under the app/libs folder. Some external jars are:

1. activation.jar
2. additionnal.jar
3. aws-android-sdk-core-2.2.1.jar
4. aws-android-sdk-ddb-2.2.1.jar
5. aws-android-sdk-ddb-mapper-2.2.1.jar
6. javacsv.jar
7. mail1.jar

These are required for AWS and Gmail services. 

The project initializes from AndroidManifest.xml . We specify all the activities within this file as well as the launcher class – MainActivity.

The Flow of activity is as under

1.	MainActivity  - The launcher class that is used for Google+ authentication. When a user authenticates, the proceed button is enabled. This launches a validate.xml which asks a user for an option of either find parking or validate

2.	MapActivity – used as a frontend for the find parking option. onCreate, we load a map fragment, obtain users coordinates and zoom in to that location. We also create an object of DynDBS which is a helper class for our DynamoDB. This is used to access the DB for the parking providers. A query is made using degraded latitudes and we check the distance of each of these markers using Haversines formula. This is stored as a map and we iterate through it to return the closest 2 markers. These markers are plotted on the map. The user can repeat the procedure for alternate locations by clicking elsewhere on the map

When a user clicks on the marker, we show an infowindow displaying the vendor name and rates and also make a toast of the current availability. Upon clicking the infowindow, we enable a masked wallet fragment to provide a Buy with Google button. If the user clicks on this button, Google’s masked wallet request is generated and card is authenticated. If successful, we provide a confirm button to create a full wallet fragment. If the user clicks on the confirm button, we call the SendMail class to send a confirmation mail to the user and vendor. We also update the ConfirmDB to log the record and move to the ThankYou class. 
3.	MapValidate – This is the frontend for the validate option. onCreate, we load a map fragment, obtain users coordinates and zoom in to that location. A query is made using secondary indexes to speed the option up and check for distances to the closest markers based on these. We identify the closest markers and return them to the class which proceeds to plot on a map. When a user click on the Marker, we display the rule pertaining to that sign. We also use the Obstacle class to parallel check for obstacles in the obstacleDB. If we are within 7m of an obstacle, the obstacle is returned and an alert is created for the user.

There are 14 classes in this project. They perform the following functions:

1.	MainActivity – launcher class for user authentication

Find Parking related:

2.	MapActivity – frontend for Find Parking.
3.	DynDBs – DynamoDB helper for Find Parking. Used to query MarkerDB and retrieve closest providers
4.	Distance – Helper class using Haversines formula to evaluate distance
5.	DBMarkers -  Mapper class to access data members within the MarkerDB
6.	DBConfirmed – Mapper class to access data members withing the ConfirmDB
7.	SendMailTLS – Helper class to send mail using TLD upon successful parking booking.
8.	Mail – Helper class for the Send Mail TLS
9.	ThankYou – booking confirmation page

Validate Parking related:

10.	MapValidate – frontend for Validate parking
11.	Validate_Main – retrieve user coordinates and perform parallel obstacle check and sign retrieval
12.	Dynamo – Helper class to access ObstacleDB and Condition table
13.	Condition_Table – used to read CSV/Dynamo rules and identify closest rules to particular location

14.	PopupAdapter – Helper class to enable inflaters on marker clicks

Demo Links :
1. Emulator version - https://www.youtube.com/watch?v=MK1D6US1gZc
2. Actual phone -  https://www.youtube.com/watch?v=4cycp6KpJ8o
