package com.google.android.gms.plus.sample.quickstart;
import android.content.Intent;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import java.util.Map;

/**
 * Created by nachirau on 4/21/15.
 */

//This indicates marker locations on map

@DynamoDBTable(tableName = "MarkerDB1")
public class DBMarkers {
    private String Serial;
    private String Latitude;
    private String DegLat; //search by this
    private String Longitude;
    private String Name; //name
    private String Mail; //mail
    private int Available;
    private int Rate;

    @DynamoDBAttribute(attributeName = "Name")
    public void setName(String Name){this.Name=Name;    }
    public String getName() {
        return Name;
    }

    @DynamoDBHashKey(attributeName = "Serial")
    public String getSerial() {
        return Serial;
    }
    public void setSerial(String serial) {
        this.Serial=serial;
    }

    @DynamoDBAttribute(attributeName = "Latitude")
    public void setLatitude(String lat){
        this.Latitude=lat;
    }
    public String getLatitude() { return Latitude; }

    @DynamoDBAttribute(attributeName = "Longitude")
    public void setLongitude(String longitude){
        this.Longitude=longitude;
    }
    public String getLongitude() {
        return Longitude;
    }

    @DynamoDBAttribute(attributeName = "degLat")
    public void setDegLat(String degLat){
        this.DegLat=degLat;
    }
    public String getDegLat() {
        return DegLat;
    }

    @DynamoDBAttribute(attributeName = "Rate")
    public void setRate(int rate){
        this.Rate=rate;
    }
    public int getRate() {
        return Rate;
    }

    @DynamoDBAttribute(attributeName = "Available")
    public void setAvailable(int available){
        this.Available=available;
    }
    public int getAvailable() {
        return Available;
    }

    @DynamoDBAttribute(attributeName = "Mail")
    public void setMail(String mail){
        this.Mail=mail;
    }
    public String getMail() {
        return Mail;
    }
}
