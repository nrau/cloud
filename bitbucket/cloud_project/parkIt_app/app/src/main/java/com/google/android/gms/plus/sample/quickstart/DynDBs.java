package com.google.android.gms.plus.sample.quickstart;

import android.content.Context;
import android.util.Log;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nachirau on 4/23/15.
 */
public class DynDBs {
    private static DBConfirmed dbConfirmed;
    private static Distance distance;
    private static int[] max;
    private static DBMarkers[] best;
    private static double[] dist;
    public static int size = 2;
    private static AmazonDynamoDBClient ddbClient;
    private static boolean[] checked;
    private static int rec_size;
    private static CognitoCachingCredentialsProvider credentialsProvider;
    private static DynamoDBMapper mapper;

    public void checkDBs() {
        credentialsProvider = new CognitoCachingCredentialsProvider(
                MapActivity.getContext(), // Context
                "IDENTITY_POOL_ID", // Identity Pool ID
                Regions.US_EAST_1 // Region
        );

        ddbClient = new AmazonDynamoDBClient(credentialsProvider);
        Log.d("In DynDB::checkDBs", "Success init");
        distance = new Distance();
        mapper = new DynamoDBMapper(ddbClient);

        max = new int[size];
        for(int i=0;i<size;i++) max[i]=10000;
    }

    public DBMarkers[] getMarkers(LatLng myLoc) {
        double myLat = myLoc.latitude;
        double myLong = myLoc.longitude;

        //crash the latitude for DB query
        int temp = (int) (myLat * 100);
        String degLat = String.valueOf(temp / 100d);
        Log.d("DynDBS::search for: ", degLat);

        ScanRequest scanRequest = new ScanRequest()
                .withTableName("MarkerDB1")
                .withAttributesToGet("Name", "Serial", "Latitude","Mail",
                        "Longitude", "degLat", "Rate", "Available");
        HashMap<String, Condition> scanFilter = new HashMap<String, Condition>();

        //Conditions
        Condition condition_degLat = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(degLat));
        scanFilter.put("degLat", condition_degLat);

        Condition condition_avl = new Condition()
                .withComparisonOperator(ComparisonOperator.GT)
                .withAttributeValueList(new AttributeValue().withN("1"));
        scanFilter.put("Available", condition_avl);

        scanRequest.withScanFilter(scanFilter);
        ScanResult result = ddbClient.scan(scanRequest);

        rec_size=result.getCount();
        if(rec_size<size) {
            rec_size=0;
            scanFilter.remove("degLat");
            scanRequest.withScanFilter(scanFilter);
            result=ddbClient.scan(scanRequest);
            rec_size=result.getCount();
        }

        Log.d("DynDBS","received "+String.valueOf(rec_size));
        dist=new double[rec_size];
        best = new DBMarkers[size];

        int i=0;
        double markerLat, markerLong, currentDistance;
        for (Map<String, AttributeValue> item : result.getItems()) {
            Log.d("++++", "ENTRY");
            Log.d("Name", item.get("Name").getS());
            Log.d("Lat", item.get("Latitude").getS());
            Log.d("Long", item.get("Longitude").getS());
            Log.d("Rate", item.get("Rate").getS());
            Log.d("Avail", item.get("Available").getN());

            //Check and load distances
            markerLat=Double.valueOf(item.get("Latitude").getS());
            markerLong=Double.valueOf(item.get("Longitude").getS());
            dist[i]= distance.getDistance(markerLat,markerLong,myLat,myLong);
            Log.d("distance", String.valueOf(dist[i]));
            i++;
        }

        List<Map<String,AttributeValue>> allResults = result.getItems();
        Map<String,AttributeValue> tempMarker;

        for(int j=0;j<size;j++) {
            int min = minMarker();
            tempMarker=allResults.get(min);
            best[j]=returnItem(tempMarker);
        }
        return best;
    }

    //Find min distance marker
    public static int minMarker() {
        int minIndex=0;
        double min=dist[0];
        for(int i=0;i<rec_size;i++) {
            if(dist[i]<min) {
                min = dist[i];
                minIndex = i;
            }
        }
        dist[minIndex]=10000;
        return minIndex;
    }

    public DBMarkers returnItem(Map<String,AttributeValue> item){
        DBMarkers temp = new DBMarkers();
        temp.setLatitude(item.get("Latitude").getS());
        temp.setLongitude(item.get("Longitude").getS());
        temp.setName(item.get("Name").getS());
        temp.setDegLat(item.get("degLat").getS());
        temp.setRate(Integer.valueOf(item.get("Rate").getS()));
        temp.setAvailable(Integer.valueOf(item.get("Available").getN()));
        temp.setMail(item.get("Mail").getS());
        temp.setSerial(item.get("Serial").getS());
        return temp;
    }

    //Add to ConfirmDB
    public boolean addConfirmDB(String Serial,String Customer,String vendorName,String Confirmation) {
        dbConfirmed = new DBConfirmed();
        dbConfirmed.setConfirmNo(Confirmation);
        dbConfirmed.setCustomer(Customer);
        dbConfirmed.setVendor(vendorName);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48
        dbConfirmed.setTime(dateFormat.format(date));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();

        String pres_count = String.valueOf(mapper.count(DBConfirmed.class, scanExpression));
        dbConfirmed.setSerial(String.valueOf(mapper.count(DBConfirmed.class,scanExpression)+1));
        Log.d("dynDBS","Count :" + pres_count);
        mapper.save(dbConfirmed);
        Log.d("dynDBS","Save Done");
        decCounter();
        return true;
    }

    //Decrement Availability
    public void decCounter() {

        ScanRequest scanRequest = new ScanRequest()
                .withTableName("MarkerDB1")
                .withAttributesToGet("Name", "Serial", "Latitude","Mail",
                        "Longitude", "degLat", "Rate", "Available");
        HashMap<String, Condition> scanFilter = new HashMap<String, Condition>();

        //Conditions
        Condition condition_serial = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(MapActivity.Serial));

        scanFilter.put("Serial", condition_serial);
        scanRequest.withScanFilter(scanFilter);

        ScanResult result = ddbClient.scan(scanRequest);
        Map<String,AttributeValue> avlMap = result.getItems().get(0);

        int value = Integer.valueOf(avlMap.get("Available").getN())-1;
        Log.d("dynDBS AVL-1 ::", String.valueOf(value));

        AttributeValue newValue = new AttributeValue().withN(String.valueOf(value));
        avlMap.put("Available",newValue);
        PutItemRequest itemRequest = new PutItemRequest().withItem(avlMap).withTableName("MarkerDB1");
        ddbClient.putItem(itemRequest);

        Log.d("decC","Decrement complete");

    }
}