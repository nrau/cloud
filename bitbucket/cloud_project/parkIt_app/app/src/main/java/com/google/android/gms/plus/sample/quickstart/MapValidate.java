package com.google.android.gms.plus.sample.quickstart;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by nachirau on 4/28/15.
 */
public class MapValidate extends FragmentActivity implements
        GoogleMap.OnInfoWindowClickListener {
    private SupportMapFragment mapFragment;
    private static LatLng myLoc;// = new LatLng(40.786621, -73.971892);
    private static Location myLocation;
    private static boolean maploaded;
    private static GoogleMap map;
    private static Validate_Main validate_main;
    public static Context thisContext;

    private static Set<String> serialnumbers;
    public static String display;
    private static Map<String,HashMap<String,AttributeValue>> result;
    private static TextView obstacleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        maploaded=false;
        super.onCreate(savedInstanceState);
        MapValidate.thisContext = getApplicationContext();
        setContentView(R.layout.validate_activity);
        obstacleText = (TextView) findViewById(R.id.obstacleText);
        obstacleText.setText("No obstacles found");
        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_valid));
        if (mapFragment != null) {
            mapFragment.getMapAsync(new OnMapReadyCallback() {

                @Override
                public void onMapReady(GoogleMap map1) {
                    map=map1;
                    Log.d("MAPSET","##########1");
                    loadMap(map1);
                    if(!maploaded) {
                        GoogleApiClient testClient = MainActivity.mGoogleApiClient;
                        myLocation = LocationServices.FusedLocationApi.getLastLocation(testClient);
                        myLoc = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        CameraUpdate center = CameraUpdateFactory.newLatLng(myLoc);
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(18);
                        map1.moveCamera(center);
                        map1.animateCamera(zoom);
                        map1.setMyLocationEnabled(true);
                        validate_main= new Validate_Main();
                        new getMarkerThread().execute();
                        maploaded=true;
                    }
                }
            });
        } else {
            Toast.makeText(this, "Error - Map Fragment was null!!", Toast.LENGTH_SHORT).show();
        }
    }

    public static Context getContext() {
        return MapValidate.thisContext;
    }

    public void addMarker(String title, String snippet, LatLng latLng) {
        if(maploaded) {
            Log.d("latLngrecv ::",latLng.toString());
            map.addMarker(new MarkerOptions()
                            .title(title)
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
            );
            Log.d("AddMarker", "Marker Added");
        }
    }

    protected void loadMap(GoogleMap googleMap) {
        map = googleMap;
        Log.d("loadMap","in Loadmap");
        if (map != null) {
            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    obstacleText.setText("");
                    myLoc = latLng;
                    new getMarkerThread().execute();

                    CameraUpdate center =
                            CameraUpdateFactory.newLatLng(latLng);
                    map.moveCamera(center);
                }
            });
            map.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater(),1));
            map.setOnInfoWindowClickListener(this);

        } else {
            Toast.makeText(this, "Error - Map was null!!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Toast.makeText(this, marker.getTitle(), Toast.LENGTH_SHORT).show();
        if(marker.getTitle().equals("obstacle")) return;
        TextView rule = (TextView) findViewById(R.id.ruleText);
        for(String serial : serialnumbers) {
            if(serial==null) continue;
            if(result.get(serial).get("Sign").getS().equals(marker.getTitle())) {
                rule.setText(result.get(serial).get("Condition").getS());
                rule.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        }
    }

    /*
     * Called when the Activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
    }

    /*
     * Called when the Activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();}

    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {

        // Global field to contain the error dialog
        private Dialog mDialog;

        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    private class getMarkerThread extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.d("inBGND","CLicked");
            try {

                result = validate_main.Validate(myLoc.latitude,myLoc.longitude);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result1) {
            Log.d("Map","Clear");
            map.clear();
            if(!result1) return;
            if(result==null) return;
            if(result.containsKey("Obstacle")) {
                display="Obstacle Nearby,Do Not Park";
                obstacleText.setText(display);

                HashMap<String,AttributeValue> obstLatLng = result.get("Obstacle");
                Double obsLat = Double.valueOf(obstLatLng.get("Lat").getS());
                Double obsLng = Double.valueOf(obstLatLng.get("Long").getS());
                LatLng latLng = new LatLng(obsLat,obsLng);
                Log.d("Obstacle","Added");
                map.addMarker(new MarkerOptions()
                                .position(latLng)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.fire))
                                .title("obstacle")

                );
            }

            Log.d("inBGND","Markers received");
            serialnumbers = result.keySet();
            Log.d("keySet",serialnumbers.toString());
            for(String serial : serialnumbers) {
                if(serial.equals("Obstacle")) continue;
                if(serial==null) continue;
                Log.d("result", "New Entry");
                Log.d("Serial",serial);
                Map<String,AttributeValue> newItem = result.get(serial);
                if(newItem==null) continue;
                if(!newItem.containsKey("Latitude") || !newItem.containsKey("Longitude") ||
                        !newItem.containsKey("Sign")) continue;
                String latMarker = newItem.get("Latitude").getS();
                String longMarker = newItem.get("Longitude").getS();
                LatLng newLatLng = new LatLng(Double.valueOf(latMarker),Double.valueOf(longMarker));
                String title = newItem.get("Sign").getS();
                addMarker(title,null,newLatLng);
            }
        }
    }
}