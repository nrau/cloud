package com.google.android.gms.plus.sample.quickstart;

/**
 * Created by nachirau on 4/28/15.
 */
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Attr;

public class Validate_Main {

    static Double user_lat = 40.737580; // USER INPUT LATITUDE
    static Double user_long = -73.982497; // USER INPUT LONGITUDE

    private static Map<String,HashMap<String,AttributeValue>> ParkingSigns = new HashMap<String,HashMap<String,AttributeValue>>();

    public static Map<String,HashMap<String,AttributeValue>> Validate(Double user_lt,Double user_lng) throws Exception{

        user_lat=user_lt;
        user_long=user_lng;

        // CHECK OBSTACLE TABLE
        System.out.println("VALID USER CO-ORDINATES !! \n");
        System.out.println("CHECKING FOR OBSTACLES.... \n");
        Dynamo obstacletable = new Dynamo();
        LatLng response = obstacletable.lookforObstacle(user_lat,user_long);

        // CHECK CONDITION TABLE
        System.out.println("CHECKING FOR PARKING SIGNS.... \n");
        Condition_Table ct = new Condition_Table();
        ct.init();
        String lat1 = Double.toString(user_lat);
        String long1 = Double.toString(user_long);

        double tempLat = user_lat*100;
        int tempLat1 = (int) tempLat;
        tempLat = tempLat1/100d;
        String user_latitude = String.valueOf(tempLat);

        double tempLng = user_lat*100;
        int tempLng1 = (int) tempLng;
        tempLng = tempLng1/100d;
        String user_longitude = String.valueOf(tempLng);

        ParkingSigns = ct.validateParking(user_latitude,user_longitude, lat1, long1);

        if (response !=null) {
            System.out.println("Obstacle Nearby,Do Not Park");
            AttributeValue tempLatit = new AttributeValue(String.valueOf(response.latitude));
            AttributeValue tempLngit = new AttributeValue(String.valueOf(response.longitude));
            HashMap<String,AttributeValue> tempLatLng = new HashMap<>();
            tempLatLng.put("Lat",tempLatit);
            tempLatLng.put("Long",tempLngit);
            ParkingSigns.put("Obstacle",tempLatLng);
        }

        Set<String> serialnumbers = new HashSet<String>();
        serialnumbers = ParkingSigns.keySet();
        if(ParkingSigns.isEmpty() || serialnumbers.isEmpty()){
            System.out.println("NO VALID PARKING SIGNS FOUND \n");
        }
        else{
            for(String s : serialnumbers) {
                if (ParkingSigns.get(s) != null)
                {
                    System.out.println("\n FOUND PARKING SIGN \n");
                    System.out.println(ParkingSigns.get(s));
                }
            }
        }
        return ParkingSigns; //String here
    }
}