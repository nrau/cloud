package com.google.android.gms.plus.sample.quickstart;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by nachirau on 4/9/15.
 */
public class ThankYou extends MapActivity {

    private String confirmation;
    private TextView confirmation_holder;
    private static DynDBs dynDBs;
    private static String[] details;
    private static boolean status=false;
    private static int count=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.thank_you);
        dynDBs = new DynDBs();
        details = MapActivity.getDetails();
        new updateDBS().execute();
        confirmation = returnConfirm();
        confirmation_holder = (TextView) findViewById(R.id.con_no);

        confirmation_holder.setText(String.format(getResources().getString(R.string.confirmation_no), confirmation));
        Log.d("ThankYou", "set confirmation");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void changeToHome(View view) {
        GoogleApiClient client = MainActivity.mGoogleApiClient;
        client.disconnect();
        setContentView(R.layout.validate);
    }

    public void changeIntentMap(View view) {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    private class updateDBS extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            count++;
            Log.d("ThankYou","inBGND");
            String vendorName = details[0];
            String Customer = details[1];
            String Confirmation = details[3];

            String Serial = String.valueOf(count);
            //Add to Confirm DB
            dynDBs.addConfirmDB(Serial,Customer,vendorName,Confirmation);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            return;
        }
    }
}
