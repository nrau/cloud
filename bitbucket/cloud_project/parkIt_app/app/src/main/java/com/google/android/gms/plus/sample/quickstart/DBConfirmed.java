package com.google.android.gms.plus.sample.quickstart;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;

/**
 * Created by nachirau on 4/23/15.
 */

//This indicates confirmed requests
@DynamoDBTable(tableName = "ConfirmDB")
public class DBConfirmed {
    private String Serial;
    private String Customer; //email
    private String ConfirmNo; //confirmation no
    private String Vendor; //mail
    private String time;

    @DynamoDBAttribute(attributeName = "ConfirmNo")
    public void setConfirmNo(String confirmNo){
        this.ConfirmNo=confirmNo;
    }
    public String getConfirmNo() {
        return ConfirmNo;
    }

    @DynamoDBHashKey(attributeName = "Serial")
    public String getSerial() {
        return Serial;
    }
    public void setSerial(String serial) {
        this.Serial=serial;
    }

    @DynamoDBAttribute(attributeName = "Vendor")
    public void setVendor(String vendor){
        this.Vendor=vendor;
    }
    public String getVendor() {
        return Vendor;
    }

    @DynamoDBAttribute(attributeName = "Customer")
    public void setCustomer(String customer){
        this.Customer=customer;
    }
    public String getCustomer() {
        return Customer;
    }

    @DynamoDBAttribute(attributeName = "Time")
    public void setTime(String time){
        this.time=time;
    }
    public String getTime() {
        return time;
    }
}
