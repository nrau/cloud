package com.google.android.gms.plus.sample.quickstart;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.wallet.Cart;
import com.google.android.gms.wallet.FullWallet;
import com.google.android.gms.wallet.FullWalletRequest;
import com.google.android.gms.wallet.LineItem;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.NotifyTransactionStatusRequest;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.gms.wallet.fragment.BuyButtonText;
import com.google.android.gms.wallet.fragment.Dimension;
import com.google.android.gms.wallet.fragment.SupportWalletFragment;
import com.google.android.gms.wallet.fragment.WalletFragmentInitParams;
import com.google.android.gms.wallet.fragment.WalletFragmentMode;
import com.google.android.gms.wallet.fragment.WalletFragmentOptions;
import com.google.android.gms.wallet.fragment.WalletFragmentStyle;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;


import java.util.List;
import java.util.UUID;

public class MapActivity extends FragmentActivity implements

        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, OnInfoWindowClickListener
{

    private static boolean clicked = false;
    private SupportMapFragment mapFragment;
    private static GoogleMap map;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 60000; /* 60 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */

    private static DynDBs dynDBs;

    private static boolean Markerset;
    private static DBMarkers[] dbMarkers;
    private static String vendor="vendor1.androidtest@gmail.com"; //vendor mail
    private static int size;
    private static Location myLocation;
    private static LatLng myLoc;

    private static LatLng chosen;
    private SupportWalletFragment mWalletFragment;
    private MaskedWallet mMaskedWallet;
    private static boolean maploaded;
    private static String vendorName; //Vendor Name
    public static String Serial;
    private static String Customer;
    private static String confirmation="ABCDEF";

    public static final int MASKED_WALLET_REQUEST_CODE = 888;
    public static Context thisContext;
    //Full wallet stuff
    public static final int FULL_WALLET_REQUEST_CODE = 889;
    private FullWallet mFullWallet;
    public static final String WALLET_FRAGMENT_ID = "wallet_fragment";
    /*
     * Define a request code to send to Google Play services This code is
     * returned in Activity.onActivityResult
     */
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        maploaded=false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        MapActivity.thisContext = getApplicationContext();
        dynDBs = new DynDBs();
        dynDBs.checkDBs();

        size = DynDBs.size;
        Markerset=false;

        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        if (mapFragment != null) {
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    loadMap(map);
                    if(!maploaded) {
                        GoogleApiClient testClient = MainActivity.mGoogleApiClient;
                        myLocation = LocationServices.FusedLocationApi.getLastLocation(testClient);
                        if(myLocation!=null)
                        myLoc = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        else
                        myLoc = new LatLng(40.786621, -73.971892);
                        CameraUpdate center = CameraUpdateFactory.newLatLng(myLoc);
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
                        map.moveCamera(center);
                        map.animateCamera(zoom);
                        map.setMyLocationEnabled(true);
                        new getMarkerThread().execute();
                        maploaded=true;
                    }
                }
            });
        } else {
            Toast.makeText(this, "Error - Map Fragment was null!!", Toast.LENGTH_SHORT).show();
        }

        mWalletFragment = (SupportWalletFragment) getSupportFragmentManager().findFragmentByTag(WALLET_FRAGMENT_ID);

        WalletFragmentInitParams startParams;
        WalletFragmentInitParams.Builder startParamsBuilder = WalletFragmentInitParams.newBuilder()
                .setMaskedWalletRequest(generateMaskedWalletRequest())
                .setMaskedWalletRequestCode(MASKED_WALLET_REQUEST_CODE);

        startParams = startParamsBuilder.build();

        if(mWalletFragment == null) {
            WalletFragmentStyle walletFragmentStyle = new WalletFragmentStyle()
                    .setBuyButtonText(BuyButtonText.BUY_WITH_GOOGLE)

                    .setBuyButtonWidth(Dimension.WRAP_CONTENT);

            WalletFragmentOptions walletFragmentOptions = WalletFragmentOptions.newBuilder()
                    .setEnvironment(WalletConstants.ENVIRONMENT_SANDBOX)
                    .setFragmentStyle(walletFragmentStyle)
                    .setTheme(WalletConstants.THEME_HOLO_LIGHT)
                    .setMode(WalletFragmentMode.BUY_BUTTON)
                    .build();

            mWalletFragment = SupportWalletFragment.newInstance(walletFragmentOptions);

            mWalletFragment.initialize(startParams);

        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Wallet.API, new Wallet.WalletOptions.Builder()
                        .setEnvironment(WalletConstants.ENVIRONMENT_SANDBOX)
                        .setTheme(WalletConstants.THEME_HOLO_LIGHT)
                        .build())
                .build();
    }

    public static Context getContext() {
        return MapActivity.thisContext;
    }

    private void loadFragment() {

        Log.i("android", "in load fragment");

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.wallet_button_holder, mWalletFragment, WALLET_FRAGMENT_ID)
                .commit();

    }

    private void clearFragment() {
        Log.i("android","in clear fragment");
        Button FullWalletButton = (Button) findViewById(R.id.full_wallet_resp);
        if(FullWalletButton.getVisibility()==View.VISIBLE) {

            Log.i("visibility_test", "setting invisible");
            FullWalletButton.setVisibility(View.INVISIBLE);
        }

        getSupportFragmentManager().beginTransaction()
                .remove(mWalletFragment)
                .commit();
    }

    private MaskedWalletRequest generateMaskedWalletRequest() {
        MaskedWalletRequest maskedWalletRequest =
                MaskedWalletRequest.newBuilder()
                        .setMerchantName("VendorA")
                        .setPhoneNumberRequired(true)
                        .setShippingAddressRequired(true)
                        .setCurrencyCode("USD")
                        .setShouldRetrieveWalletObjects(true)
                        .setEstimatedTotalPrice("10.00")
                        .setCart(Cart.newBuilder()
                                .setCurrencyCode("USD")
                                .setTotalPrice("10.00")
                                .addLineItem(LineItem.newBuilder()
                                        .setCurrencyCode("USD")
                                        .setDescription("Payment test")
                                        .setQuantity("1")
                                        .setUnitPrice("10.00")
                                        .setTotalPrice("10.00")
                                        .build())
                                .build())
                        .build();

        return maskedWalletRequest;
    }

    private FullWalletRequest generateFullWalletRequest(String googleTransactionId) {
        //Add to Request DB
        FullWalletRequest fullWalletRequest = FullWalletRequest.newBuilder()
                .setGoogleTransactionId(googleTransactionId)
                .setCart(Cart.newBuilder()
                        .setCurrencyCode("USD")
                        .setTotalPrice("10.10")
                        .addLineItem(LineItem.newBuilder()
                                .setCurrencyCode("USD")
                                .setDescription("Payment test")
                                .setQuantity("1")
                                .setUnitPrice("10.00")
                                .setTotalPrice("10.00")
                                .build())
                        .addLineItem(LineItem.newBuilder()
                                .setCurrencyCode("USD")
                                .setDescription("Tax")
                                .setRole(LineItem.Role.TAX)
                                .setTotalPrice(".10")
                                .build())
                        .build())
                .build();
        return fullWalletRequest;
    }

    public void requestFullWallet(View view) {
        Wallet.Payments.loadFullWallet(mGoogleApiClient,
                generateFullWalletRequest(mMaskedWallet.getGoogleTransactionId()),
                FULL_WALLET_REQUEST_CODE);
        Log.i("Full wallet", "generated request");
    }

    public void addMarker(String title,String snippet, LatLng latLng) {
        map.addMarker(new MarkerOptions()
                        .title(title)
                        .snippet(snippet)
                        .position(latLng)
        );
    }

    protected void loadMap(GoogleMap googleMap) {
        map = googleMap;
        if (map != null) {
            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(0,0));
            map.moveCamera(center);
            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    if(clicked) {
                        clearFragment();
                        clicked=false;
                    }
                    Button fullWallet = (Button) findViewById(R.id.full_wallet_resp);
                    fullWallet.setVisibility(View.INVISIBLE);
                    myLoc = latLng;
                    new getMarkerThread().execute();

                    CameraUpdate center =
                            CameraUpdateFactory.newLatLng(latLng);
                    map.moveCamera(center);
                }
            });
            map.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater(),0));
            map.setOnInfoWindowClickListener(this);

        } else {
            Toast.makeText(this, "Error - Map was null!!", Toast.LENGTH_SHORT).show();
        }
    }

    public void setVendor(Marker marker) {
        for(int i =0;i<dbMarkers.length;i++) {
            if(dbMarkers[i].getName().equals(marker.getTitle())) {
                Serial = dbMarkers[i].getSerial();
                vendor=dbMarkers[i].getMail();
                Log.d("MAILSET","set vendor :"+vendor);
                vendorName=marker.getTitle();
                Customer=MainActivity.user_name;
            }
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if(!clicked) {
            loadFragment();
            for(int i=0;i<dbMarkers.length;i++) {
                if(dbMarkers[i]!=null) {
                    if(dbMarkers[i].getName().equals(marker.getTitle()))
                        Toast.makeText(this, "Available :" + dbMarkers[i].getAvailable(), Toast.LENGTH_LONG).show();
                }
            }
            setVendor(marker);
            clicked = true;
        }
        chosen=marker.getPosition();
    }

    protected void connectClient() {
        if (isGooglePlayServicesAvailable() && mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    /*
     * Called when the Activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
        connectClient();
    }

    /*
     * Called when the Activity is no longer visible.
     */
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();}
    /*
     * Handle results returned to the FragmentActivity by Google Play services
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {

            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
                /*
                 * If the result code is Activity.RESULT_OK, try to connect again
                 */
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        mGoogleApiClient.connect();
                        break;
                }
                break;
            case MASKED_WALLET_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        mMaskedWallet =
                                data.getParcelableExtra(WalletConstants.EXTRA_MASKED_WALLET);
                        Button FullWalletButton = (Button) findViewById(R.id.full_wallet_resp);
                        if(FullWalletButton.getVisibility()==View.INVISIBLE) {

                            Log.i("visibility_test", "setting visible");
                            FullWalletButton.setVisibility(View.VISIBLE);
                        }
                        else {
                            Log.i("visibility test", "failed");
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                    default:
                        Toast.makeText(this, "An Error Occurred", Toast.LENGTH_LONG).show();
                        break;
                }
                break;
            case FULL_WALLET_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("Full wallet request","OK");
                        String temp = UUID.randomUUID().toString();
                        this.confirmation = temp.substring(0, Math.min(temp.length(), 10));
                        mFullWallet =
                                data.getParcelableExtra(WalletConstants.EXTRA_FULL_WALLET);
                        if(mFullWallet.getProxyCard().getPan().toString()!=null) {
                            Toast.makeText(this,"Order Successful",Toast.LENGTH_LONG).show();
                            new SendMailTLS(MainActivity.user_name, null, this.confirmation,chosen).execute();
                            sendCardDetails();
                        }
                        Wallet.Payments.notifyTransactionStatus(mGoogleApiClient,
                                generateNotifyTransactionStatusRequest(mFullWallet.getGoogleTransactionId(),
                                        NotifyTransactionStatusRequest.Status.SUCCESS));
                        mGoogleApiClient.disconnect();
                        Intent intent = new Intent(this,ThankYou.class);
                        startActivity(intent);
                        break;
                    default:
                        Toast.makeText(this, "An Error Occurred", Toast.LENGTH_LONG).show();
                        break;
                }
                break;
            case WalletConstants.RESULT_ERROR:
                Toast.makeText(this, "An Error Occurred", Toast.LENGTH_LONG).show();
                break;
        }
    }

    public String returnConfirm(){
        return this.confirmation;
    }

    private boolean isGooglePlayServicesAvailable() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates", "Google Play services is available.");
            return true;
        } else {
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(errorDialog);
                errorFragment.show(getSupportFragmentManager(), "Location Updates");
            }

            return false;
        }
    }

    /*
     * Called by Location Services when the request to connect the client
     * finishes successfully. At this point, you can request the current
     * location or start periodic updates
     */
    @Override
    public void onConnected(Bundle dataBundle) {
    }

    public static NotifyTransactionStatusRequest generateNotifyTransactionStatusRequest(
            String googleTransactionId, int status) {
        return NotifyTransactionStatusRequest.newBuilder()
                .setGoogleTransactionId(googleTransactionId)
                .setStatus(status)
                .build();
    }

    public String getConfirmation() {
        return confirmation;
    }

    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
    }

    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    /*
     * Called by Location Services if the connection to the location client
     * drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
        } else if (i == CAUSE_NETWORK_LOST) {
            Toast.makeText(this, "Network lost. Please re-connect.", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * Called by Location Services if the attempt to Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects. If the error
         * has a resolution, try sending an Intent to start a Google Play
         * services activity that can resolve error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry. Location services not available to you", Toast.LENGTH_LONG).show();
        }
    }

    public static String[] getDetails() {
        String[] details = new String[4];
        details[0] = vendorName;
        details[1] = Customer;
        details[2] = Serial;
        details[3] = confirmation;
        return details;
    }

    public void sendCardDetails() {

        String card_details = "\nTransactionID(Google) : " +  mFullWallet.getGoogleTransactionId()
                + "\nExp Month : " + Integer.toString(mFullWallet.getProxyCard().getExpirationMonth())
                + "\nExp Year : " + Integer.toString(mFullWallet.getProxyCard().getExpirationYear())
                + "\nCVN : " + mFullWallet.getProxyCard().getCvn()
                + "\nBilling Address St : \n" + mFullWallet.getBuyerBillingAddress().getAddress1()
                + "\n" + mFullWallet.getBuyerBillingAddress().getAddress2()
                + "\n" + mFullWallet.getBuyerBillingAddress().getLocality()
                + "\nEmail : " + MainActivity.user_name  ;

        //Send vendor mail
        new SendMailTLS(1,vendor,confirmation,card_details).execute();
    }

    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {

        // Global field to contain the error dialog
        private Dialog mDialog;

        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }

    }

    private class getMarkerThread extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            dbMarkers=dynDBs.getMarkers(myLoc);
            Log.d("inBGND","Markers recvd");
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (dbMarkers.length==0) return;
            if (dbMarkers!=null && result) {
                for(int i=0;i<dbMarkers.length;i++) {
                    if(dbMarkers[i]!=null) {
                        if(Markerset) {
                            map.clear();
                            Markerset=false;
                        }
                        Log.d("Marker rcd ::: NEW -- ", "#######");
                        Log.d("Marker rcd ::: Lat ", dbMarkers[i].getLatitude());
                        Log.d("Marker rcd ::: Long ", dbMarkers[i].getLongitude());
                        Log.d("Marker rcd ::: Name ", dbMarkers[i].getName());
                        Log.d("Marker rcd ::: vendor ", dbMarkers[i].getMail());
                        LatLng tmp = new LatLng(Double.valueOf(dbMarkers[i].getLatitude()),
                                Double.valueOf(dbMarkers[i].getLongitude()));
                        addMarker(dbMarkers[i].getName(),String.valueOf(dbMarkers[i].getRate()),
                                tmp);
                    }
                    else
                        Log.d("Marker rcd ::: NULL -- ", "NULL");
                }
                Markerset=true;
            }
            return;
        }
    }
}
