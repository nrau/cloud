package com.google.android.gms.plus.sample.quickstart;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

class PopupAdapter implements InfoWindowAdapter {
  private View popup=null;
  private LayoutInflater inflater=null;
  private int context;

  PopupAdapter(LayoutInflater inflater, int context) {
    this.inflater=inflater;
    this.context=context;
  }

  @Override
  public View getInfoWindow(Marker marker) {
    return(null);
  }

  @SuppressLint("InflateParams")
  @Override
  public View getInfoContents(Marker marker) {
    if (popup == null) {
      popup=inflater.inflate(R.layout.popup, null);
    }

    TextView tv=(TextView)popup.findViewById(R.id.title);
    tv.setText(marker.getTitle());

    if(context==0) {
      tv = (TextView) popup.findViewById(R.id.snippet);
      tv.setText(" $" + marker.getSnippet());
    }
    return(popup);
  }
}