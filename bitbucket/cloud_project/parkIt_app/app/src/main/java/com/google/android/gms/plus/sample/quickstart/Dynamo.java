package com.google.android.gms.plus.sample.quickstart;

/**
 * Created by nachirau on 4/28/15.
 */
import java.util.Map;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.google.android.gms.maps.model.LatLng;

public class Dynamo{

    private static AmazonDynamoDBClient dynamoDB;
    private static CognitoCachingCredentialsProvider credentials;

    private static void init() throws Exception {
        credentials = new CognitoCachingCredentialsProvider(
                MapValidate.getContext(), // Context
                "INSERT_POOL_ID", // Identity Pool ID
                Regions.US_EAST_1 // Region
        );
        dynamoDB = new AmazonDynamoDBClient(credentials);
        Region usEast1 = Region.getRegion(Regions.US_EAST_1);
        dynamoDB.setRegion(usEast1);
    }

    public static AmazonDynamoDBClient myDbClient() {
        return dynamoDB;
    }

    public LatLng lookforObstacle(double userlat, double userlong) throws Exception{
        init();
        String latitude1, longitude1;
        latitude1 = "";
        longitude1 = "";
        ScanRequest scanRequest = new ScanRequest()
                .withTableName("ObstacleTable");
        ScanResult result = dynamoDB.scan(scanRequest);

        for (Map<String, AttributeValue> item : result.getItems()){
            AttributeValue lat = item.get("latitude");
            AttributeValue lon = item.get("longitude");
            AttributeValue obst = item.get("obstacle");
            String signn = obst.getS();
            latitude1 = lat.getS();
            double testlat= Double.parseDouble(latitude1);
            longitude1 = lon.getS();
            double testlong=Double.parseDouble(longitude1);
            Distance fd = new Distance();
            Double distance=fd.getDistance(userlat, userlong, testlat, testlong);
            if (distance<7)
            {
                LatLng tempLatLng = new LatLng(testlat,testlong);
                return tempLatLng; //Obstacle nearby
            }
        }
        return null; //No obstacle
    }
}
