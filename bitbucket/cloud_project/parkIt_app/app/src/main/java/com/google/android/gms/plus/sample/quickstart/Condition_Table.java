package com.google.android.gms.plus.sample.quickstart;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.csvreader.CsvReader;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by nachirau on 4/28/15.
 */
public class Condition_Table {
    private static int resultSize;
    private static int resultNo = 10;
    private static double[] allDistances;
    private static String user_lat = " ";
    private static String user_long = " ";

    private static  String userLT;
    private static String userLNG;
    private static Map<String,HashMap<String,AttributeValue>> ParkingInfo = new HashMap<String,HashMap<String,AttributeValue>>();

    public static void init() throws Exception {
        ParkingInfo.clear();
    }

    //Look for closest rules
    private static void lookPark() throws Exception {
        double userlat = Double.valueOf(userLT);
        double userlong = Double.valueOf(userLNG);
        Context context = MapValidate.getContext();
        AssetManager mgr = context.getAssets();
        InputStream inputStream = mgr.open("rules.csv");

        //Query only based on degraded latitude
        CsvReader reader = new CsvReader(new InputStreamReader(inputStream));
        int count =0;
        reader.getHeaders();
        while(reader.readRecord()) {
            String[] rec = reader.getValues();
            if (!rec[5].toString().equals(user_lat)) continue;
            count++;
        }

        //remove conditional match if query fails
        if(count<10)
        {
            AssetManager mgr2 = context.getAssets();
            InputStream inputStream1 = mgr2.open("rules.csv");
            reader = new CsvReader(new InputStreamReader(inputStream1));
            while(reader.readRecord()) {
                String[] rec = reader.getValues();
                count++;
            }
        }

        allDistances = new double[count];
        Distance distanceCalc = new Distance();

        System.out.print("COUNT = " + count+"\n");
        resultSize = count;

        AssetManager mgr1 = context.getAssets();
        inputStream = mgr1.open("rules.csv");
        CsvReader reader1 = new CsvReader(new InputStreamReader(inputStream));

        System.out.print("READ SUCCESS");

        AttributeValue[] Rule = new AttributeValue[count];
        AttributeValue[] Lat= new AttributeValue[count];
        AttributeValue[] Longit= new AttributeValue[count];
        AttributeValue[] Sign= new AttributeValue[count];
        AttributeValue[] SlNo= new AttributeValue[count];

        int i =0;
        while(reader1.readRecord()){
            String[] rec = reader1.getValues();
            if (!rec[5].toString().equals(user_lat)) continue;

            Rule[i] = new AttributeValue(rec[6]);
            Lat[i] = new AttributeValue(rec[3]);
            Longit[i] = new AttributeValue(rec[2]);
            Sign[i] = new AttributeValue(rec[1]);
            SlNo[i]= new AttributeValue(rec[0]);

            double latdb = Double.valueOf(Lat[i].getS().toString());
            double longdb = Double.valueOf(Longit[i].getS().toString());

            allDistances[i] = distanceCalc.getDistance(userlat, userlong, latdb, longdb);
            i++;
        }

        for(int k = 0;k<resultNo;k++) {
            HashMap<String, AttributeValue> bestMarkers = new HashMap<>();
            int minIndex = minMarker();
            bestMarkers.put("SlNo",SlNo[minIndex]);
            bestMarkers.put("Latitude",Lat[minIndex]);
            bestMarkers.put("Longitude",Longit[minIndex]);
            bestMarkers.put("Sign",Sign[minIndex]);
            bestMarkers.put("Condition",Rule[minIndex]);
            ParkingInfo.put(SlNo[minIndex].getS(), bestMarkers);
        }

        //Display closest markers
        Set<String> allKeys = ParkingInfo.keySet();
        Iterator<String> iterator = allKeys.iterator();
        while(iterator.hasNext()) {
            String temp = iterator.next();
            Map<String,AttributeValue> temp1 = ParkingInfo.get(temp);
            //Sysout....
        }
    }

    //Find the minimum distance marker
    private static int minMarker() {
        int minIndex=0;
        double min=10000d;
        for(int i=0;i<resultSize;i++) {
            if(allDistances[i]<min) {
                min = allDistances[i];
                minIndex = i;
            }
        }
        allDistances[minIndex]=10000d;
        return minIndex;
    }

    //Validate parking method
    public static Map<String,HashMap<String,AttributeValue>> validateParking(String user_lt,String user_lng
            ,String userLT1, String userLNG1) throws Exception {
        Condition_Table dynamotable = new Condition_Table();
        user_lat = user_lt;
        user_long = user_lng;
        userLT = userLT1;
        userLNG = userLNG1;
        dynamotable.lookPark();
        return ParkingInfo;
    }
}